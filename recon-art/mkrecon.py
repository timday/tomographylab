#!/usr/bin/env python

import cPickle as pickle
import Image
import matplotlib.pyplot as plt
import math
from multiprocessing import Pool
import numpy as np
from optparse import OptionParser
import projections
import scipy.sparse
import sys
import time

import sys
sys.path.append('../lib/')
import sparsedense

# Progress indicator
def dot():
    sys.stdout.write('.')
    sys.stdout.flush()

def sqr(x):
    return x*x

s=(256,256)    

def reconstruct(options,filename_src,filename_dst):
    # Read sinogram and normalize range
    src=Image.open(filename_src)
    if src.mode=='L':
        src=np.asarray(src)/255.0
    elif src.mode=='I;16':
        #src=np.asarray(src)/65535.0 # Doesn't work: need to do this workround instead
        src=np.fromstring(src.tostring(),np.uint16).reshape((src.size[1],src.size[0]))/65535.0
    else:
        exit('Unexpected image file type')

    # Convert back to accumulated material estimate
    k=64.0
    src = -k*np.log(src)

    t0=time.time()
    with open('projections.pkl','r') as f:
        proj=pickle.load(f)
    print 'Loaded projections.pkl: {0}'.format(proj.info())
    if proj.shape!=src.shape:
        exit('projections.pkl inconsistent with number of sinogram rows')
    t1=time.time()
    print "Loaded projections in %g seconds" % (t1-t0)

    # Iterate Kaczmarz method
    w=0.25
    estimate=np.zeros(s)
    frame=0
    for i in range(1):
        print "Iteration %d" % i
        pixels=np.array(range(proj.shape[0]*proj.shape[1]))  # Could randomize
        for pixel in pixels:
            row = pixel/proj.shape[1]
            col = pixel%proj.shape[1]
            if col==0:
                dot()
                dsti=np.uint8(np.rint(np.clip(255.0*estimate,0.0,255.0)))
                Image.fromarray(dsti).save('art-{0:06d}.png'.format(frame))
                frame=frame+1

            error=sparsedense.dot(proj.projections[pixel],estimate)-src[row,col]
            sparsedense.accumulate(
                proj.projections[pixel],
                -w*error/proj.norm2s[pixel],
                estimate
                )
            estimate=np.clip(estimate,0.0,1.0)

        print

    dsti=np.uint8(np.rint(np.clip(255.0*estimate,0.0,255.0)))
    Image.fromarray(dsti).save(filename_dst)

def main():
    parser = OptionParser()  # TODO: Upgrade to argparser with Py 2.7

    parser.add_option("-p","--plot",action="store_true",dest="plot",help="Show some plots",default=False)

    (options,args) = parser.parse_args()

    if len(args) != 2:
        sys.exit('Usage: mkrecon [options] <src> <dst>')

    reconstruct(options,args[0],args[1])

    if options.plot:
        plt.show()

if __name__ == "__main__":
    main()
