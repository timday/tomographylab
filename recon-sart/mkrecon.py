#!/usr/bin/env python

import cPickle as pickle
import Image
import matplotlib.pyplot as plt
import math
from multiprocessing import Pool
import numpy as np
from optparse import OptionParser
import projections
import scipy
import scipy.sparse
import sys
import time

import sys
sys.path.append('../lib/')
import sparsedense

# Progress indicator
def dot():
    sys.stdout.write('.')
    sys.stdout.flush()

def sqr(x):
    return x*x

s=(256,256)    

projections=None
sinogram=None

class CorrectionCalculator:
    def __init__(self,options,estimate):
        self._options=options
        self._estimate=estimate
        self._w=0.5/projections.shape[1]  # 1.0 too fast?

    def __call__(self,row):
        correction=np.zeros(s,dtype=np.float32)
        dot()
        for col in xrange(projections.shape[1]):
            pixel=row*projections.shape[1]+col
            error=sparsedense.dot(projections.projections[pixel],self._estimate)-sinogram[row,col]
            sparsedense.accumulate(
                projections.projections[pixel],
                -(self._w)*error/projections.norm2s[pixel],
                correction
                )
        return correction
        
def reconstruct(options,filename_src,filename_dst):

    global projections
    global sinogram

    # Read sinogram and normalize range
    src=Image.open(filename_src)
    if src.mode=='L':
        src=np.asarray(src)/255.0
    elif src.mode=='I;16':
        #src=np.asarray(src)/65535.0 # Doesn't work: need to do this workround instead
        src=np.fromstring(src.tostring(),np.uint16).reshape((src.size[1],src.size[0]))/65535.0
    else:
        exit('Unexpected image file type')

    # Convert back to accumulated material estimate
    k=64.0
    sinogram = np.float32(-k*np.log(src))

    t0=time.time()
    with open('projections.pkl','r') as f:
        projections=pickle.load(f)
    t1=time.time()
    print "Loaded projections.pkl in %g seconds" % (t1-t0)
    print 'Loaded projections.pkl: {0}'.format(projections.info())
    if projections.shape!=sinogram.shape:
        exit('projections.pkl inconsistent with number of sinogram rows')

    # Similar idea to ART except correction is computed independently and then the total applied
    # The old rev.103 version took 47s to compute an update with max 0.497159581066
    # We get down to ~23s with 2 processes... or 4!
    # More use of floats gets 15s for 4 or 26s for 2.
    estimate=np.zeros(s)
    rows=xrange(projections.shape[0])
    t0=time.time()
    for frame in xrange(101):
        cc=CorrectionCalculator(options,estimate)
        correction=reduce(lambda x,y: x+y,Pool().map(cc,rows))
        estimate=np.clip(estimate+correction,0.0,1.0)
        dsti=np.uint8(np.rint(np.clip(255.0*estimate,0.0,255.0)))
        Image.fromarray(dsti).save('sart-{0:06d}.png'.format(frame))
        t1=time.time()
        print t1-t0,np.min(correction),np.max(correction)
        t0=t1
        print

    dsti=np.uint8(np.rint(np.clip(255.0*estimate,0.0,255.0)))
    Image.fromarray(dsti).save(filename_dst)

def main():
    parser = OptionParser()  # TODO: Upgrade to argparser with Py 2.7

    parser.add_option("-p","--plot",action="store_true",dest="plot",help="Show some plots",default=False)

    (options,args) = parser.parse_args()

    if len(args) != 2:
        sys.exit('Usage: mkrecon [options] <src> <dst>')

    reconstruct(options,args[0],args[1])

    if options.plot:
        plt.show()

if __name__ == "__main__":
    main()
