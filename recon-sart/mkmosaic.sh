#!/bin/bash

# 1 row of 6
# F=`ls sart-??????.png | awk '{if (NR==1 || NR==2 || NR==4 || NR==8 || NR==16 || NR==32) print $0;}'`
# montage -tile 6x1 -geometry 100% -bordercolor '#ffffff' -border 1 $F recon-montage.png

F=`ls sart-??????.png | awk '{if (NR==1 || NR==2 || NR==3 || NR==4 || NR==5 || NR==7 || NR==10 || NR==14 || NR==19 || NR==26 || NR==36 || NR==50) print $0;}'`
montage -tile 6x2 -geometry 100% -bordercolor '#ffffff' -border 1 $F recon-montage.png

# 1,2,3,4,5,7,10,14,19,26,36,50
