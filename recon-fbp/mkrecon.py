#!/usr/bin/env python

import Image
import math
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from multiprocessing import Pool
import numpy as np
from optparse import OptionParser
import random
import scipy.interpolate
import scipy.ndimage.interpolation
import numpy.fft
import sys

# Progress indicator
def dot():
    sys.stdout.write('.')
    sys.stdout.flush()

S=256
C=0.5*(S-1)

# Builds filter taps as per The Book
# will be centered on n/2
def mkfiltercoeffs(n,e):
    
    def c(x):
        if x==0:
            return 1.0-2.0*e/3.0
        elif x%2==0:
            return -4.0*e/(math.pi*math.pi*x*x)
        else:
            assert x%2==1
            return -4.0*(1.0-e)/(math.pi*math.pi*x*x)
            
    return np.array(map(c,range(-n/2,n/2)))

class FilteredBackprojector:
    def __init__(self,options,sinogram,f):
        self._options=options
        self._sinogram=sinogram
        self._filter=f
        
        ir,ic = np.meshgrid(range(S),range(S))
        self._y=ir-C
        self._x=ic-C
        self._k=64.0

    def __project(self,row):
        a=(math.pi/180.0)*(row*360.0/self._sinogram.shape[0])
        return C+math.cos(a)*self._x-math.sin(a)*self._y

    def __call__(self,row):
        dot()
    
        src=-self._k*np.log(self._sinogram[row])  # Recover density
        
        if self._filter != None: src=np.real(np.fft.ifft(np.fft.fft(src)*self._filter))
        
        p=self.__project(row)
        return scipy.interpolate.interp1d(np.arange(S),src,bounds_error=False,kind='linear')(p)/self._sinogram.shape[0]

def reconstruct(options,filename_src,filename_dst):

    # Read sinogram and normalize range
    src=Image.open(filename_src)
    if src.mode=='L':
        src=np.asarray(src)/255.0
    elif src.mode=='I;16':
        #src=np.asarray(src)/65535.0 # Doesn't work: need to do this workround instead
        src=np.fromstring(src.tostring(),np.uint16).reshape((src.size[1],src.size[0]))/65535.0
    else:
        exit('Unexpected image file type')

    flt=None
    if options.filter:
        # Build filter taps as per The Book (built on n/2)
        tapsup=mkfiltercoeffs(src.shape[1],options.filter_epsilon)
        taps=np.fft.ifftshift(tapsup) # Make no phase shift (ie move to origin)
        flt=np.fft.fft(taps)        # Covert to frequency domain

        if options.plot_filename:
            fig=plt.figure(figsize=(12,4))
            fig.add_subplot(1,2,1)
            plt.bar(range(-len(tapsup)/2,len(tapsup)-len(tapsup)/2),tapsup,1.0,color='b',edgecolor='b',align='center')
            plt.xlim((-16,16))
            plt.title('Convolution filter taps for $\\epsilon$ = %g' % options.filter_epsilon)
            fig.add_subplot(1,2,2)
            plt.bar(range(-len(flt)/2,-len(flt)/2+len(taps)),np.real(np.fft.fftshift(flt)),color='b',edgecolor='b',align='center')
            plt.xlim((-128,128))
            plt.title('Filter frequency response for $\\epsilon$ = %g' % options.filter_epsilon)
            plt.subplots_adjust(left=0.1,bottom=0.1,right=0.9,top=0.9,hspace=0.4,wspace=0.2)
            plt.savefig(options.plot_filename)
            
    # Filter, backproject and accumulate over rows
    fbp=FilteredBackprojector(options,src,flt)
    dst=reduce(lambda x,y: x+y,Pool().map(fbp,range(src.shape[0])))
    print

    # Any pixels not in "acquisition disk" were tagged NaN by interpolation out of bounds
    mask=np.isnan(dst)
    # So zero them instead
    dst=np.select([mask],[np.zeros(S)],default=dst)

    #if options.plot:
    #    plt.figure()
    #    plt.imshow(dst)

    # Show some statistics
    lo,mean,hi=np.min(dst),np.mean(dst),np.max(dst)
    print 'Min %g, mean %g, max %g' % (lo,mean,hi)
    
    if options.scale or options.scalehi:
        if options.scalehi:
            dsti=np.uint8(np.rint(np.clip(255.0*dst/hi,0.0,255.0)))
        else:
            dsti=np.uint8(np.rint(np.clip(255.0*(dst-lo)/(hi-lo),0.0,255.0)))
    else:
        dsti=np.uint8(np.rint(np.clip(255.0*dst,0.0,255.0)))

    Image.fromarray(dsti).save(filename_dst)

def main():
    parser = OptionParser()  # TODO: Upgrade to argparser with Py 2.7
    parser.add_option("-u","--unfiltered",action="store_false",dest="filter",help="Don't filter",default=True)
    parser.add_option("-s","--scale",action="store_true",dest="scale",help="Scale output to range",default=False)
    parser.add_option("-S","--Scale",action="store_true",dest="scalehi",help="Scale output to range on hi-end only",default=False)
    parser.add_option("-p","--plot",dest="plot_filename",help="Create plots to given filename",type='string',default=None)
    parser.add_option("-e","--filter-epsilon",dest="filter_epsilon",help="Specify a filter epsilon value",type='float',default=0.0) # 0.0 yields the "straight" ramp filter; seems to be sharpest.

    (options,args) = parser.parse_args()

    print "Using filter epsilon %g " % options.filter_epsilon

    if len(args) != 2:
        sys.exit('Usage: mkrecon [options] <src> <dst>')

    reconstruct(options,args[0],args[1])

if __name__ == "__main__":
    main()
