#!/usr/bin/env python

import Image
import math
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from optparse import OptionParser
import scipy.interpolate
import scipy.fftpack
import scipy.ndimage.interpolation
import sys

S=256
M=1

def modfilename(filename,qualifier):
    parts=filename.split('.')
    return '.'.join(parts[:-1])+qualifier+'.'+parts[-1]

def hideaxis(ax):
    ax.set_frame_on(False)
    ax.axes.get_xaxis().set_visible(False)
    ax.axes.get_yaxis().set_visible(False)

def reconstruct(options,filename_src,filename_dst):
    
    # Read sinogram and normalize range
    src=Image.open(filename_src)
    if src.mode=='L':
        src=np.asarray(src)/255.0
    elif src.mode=='I;16':
        #src=np.asarray(src)/65535.0 # Doesn't work: need to do this workround instead
        src=np.fromstring(src.tostring(),np.uint16).reshape((src.size[1],src.size[0]))/65535.0
    else:
        exit('Unexpected image file type')

    k=64.0
    src=-k*np.log(src)  # Recover density

    assert S==src.shape[1]

    def angle(i): return 0.5*math.pi+(2.0*math.pi*i)/src.shape[0]

    # Fourier transform the rows of the sinogram, move the DC component to the row's centre
    src_fft_rows=scipy.fftpack.fftshift(
        scipy.fftpack.fft(
            scipy.fftpack.ifftshift(
                src,
                axes=1
                )
            ),
        axes=1
        )
        
    # Coordinates of sinogram FFT-ed rows' samples in 2D FFT space
    a=np.array([angle(i) for i in xrange(src.shape[0])])
    r=np.arange(S)-S/2
    r,a=np.meshgrid(r,a)
    r=r.flatten()
    a=a.flatten()
    srcx=M*((S/2)+r*np.cos(a))
    srcy=M*((S/2)+r*np.sin(a))
    
    # Coordinates of regular grid in 2D FFT space
    dstx,dsty=np.meshgrid(np.arange(M*S),np.arange(M*S))
    dstx=dstx.flatten()
    dsty=dsty.flatten()
        
    # Let the central slice theorem work its magic!
    # Interpolate the 2D Fourier space grid from the transformed sinogram rows
    fft2=scipy.interpolate.griddata(
        (srcy,srcx),
        src_fft_rows.flatten(),
        (dsty,dstx),
        method='cubic',
        fill_value=0.0
        ).reshape((M*S,M*S))
    
    if options.plot_filename:

        target=np.asarray(Image.open('../generate/target.png'))/255.0
        target_fft2=scipy.fftpack.fftshift(
            scipy.fftpack.fft2(
                scipy.fftpack.ifftshift(target)
                )
            )

        row=src[270]
        plt.figure(figsize=(9,3))
        #plt.plot(np.arange(0,S),row,color='b',marker=',')
        plt.fill_between(np.arange(0,S),row,0.0,color='b')
        plt.xlim([0,S-1])
        plt.savefig(modfilename(options.plot_filename,'-sinogram_row'),bbox_inches='tight')
        
        plt.figure(figsize=(9,3))
        row_fft=scipy.fftpack.ifftshift(
            scipy.fftpack.fft(
                scipy.fftpack.ifftshift(row)
                )
            )
        #plt.plot(np.arange(-S/2,S/2),np.abs(row_fft),color='b',marker=',')
        plt.fill_between(np.arange(-S/2,S/2),np.abs(row_fft),0.1,color='b')
        plt.xlim([-S/2,S/2-1])
        plt.gca().set_yscale('log')
        plt.savefig(modfilename(options.plot_filename,'-sinogram_row_fft'),bbox_inches='tight')

        plt.figure(figsize=(6,6))
        plt.imshow(np.abs(target_fft2),vmax=100.0)
        hideaxis(plt.gca())
        plt.savefig(modfilename(options.plot_filename,'-target_fft2'),bbox_inches='tight')

        plt.figure(figsize=(6,6))
        plt.imshow(np.abs(fft2),vmax=100.0)
        hideaxis(plt.gca())
        plt.savefig(modfilename(options.plot_filename,'-interpolated_fft2'),bbox_inches='tight')
    
    # Transform from 2D Fourier space back to a reconstruction of the target
    recon=np.real(
        scipy.fftpack.fftshift(
            scipy.fftpack.ifft2(
                scipy.fftpack.ifftshift(fft2)
                )
            )
        )

    # Crop if was oversampled
    offset=(M-1)*S/2
    recon=recon[offset:offset+S,offset:offset+S]
    

    dsti=np.uint8(np.rint(np.clip(255.0*recon,0.0,255.0)))
    
    Image.fromarray(dsti).save(filename_dst)

def main():
    parser = OptionParser()  # TODO: Upgrade to argparser with Py 2.7
    parser.add_option("-p","--plot",dest="plot_filename",help="Create plots to given filename",type='string',default=None)
    (options,args) = parser.parse_args()

    if len(args) != 2:
        sys.exit('Usage: mkrecon [options] <src> <dst>')

    reconstruct(options,args[0],args[1])

if __name__ == "__main__":
    main()
