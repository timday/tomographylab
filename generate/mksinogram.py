#!/usr/bin/env python

import Image

from multiprocessing import Pool
from optparse import OptionParser

import math
import numpy as np
import scipy.interpolate
import scipy.ndimage.interpolation
import sys

def dot():
    sys.stdout.write('.')
    sys.stdout.flush()
    
class Scanner:

    def __init__(self,tgt,n,k):
        self._tgt=tgt
        self._num_angles=n
        self._k=k

    def num_angles(self): return self._num_angles

    # Return the attenuated signal after passing through total accumulation along rows for the i-th exposure angle
    # Rotation center is 0.5*(shape-1)
    def __call__(self,i):
        assert 0<=i and i<self._num_angles
        dot()
        a=i*360.0/self._num_angles
        tgtr=scipy.ndimage.interpolation.rotate(self._tgt,a,order=1,reshape=False,mode='constant',cval=0.0)
        return np.exp(-np.sum(tgtr,axis=1)/self._k)

def main():

    parser = OptionParser()  # TODO: Upgrade to argparser with Py 2.7
    parser.add_option("-a","--angles",dest="angles",help="Use N angular samples (default 359)",type='int',metavar="N",default=359)
    parser.add_option("-p","--precise",action='store_true',dest="precise",help="Save a 16 bit image",default=False)
    parser.add_option("-k","--k-coefficient",dest="k",help="Attenuation coefficient",type='float',metavar="K",default=64.0)
    (options,args) = parser.parse_args()

    if len(args) != 2:
        sys.exit('Usage: mksinogram [options] <src.png> <dst.png>')
    
    tgt=np.asarray(Image.open(args[0]))/255.0

    scanner=Scanner(tgt,options.angles,options.k)

    pool=Pool()
    rows=pool.map(scanner,range(scanner.num_angles()))
    print

    dst=np.array(rows)
    print "Raw sinogram image range %g to %g" % (np.min(dst),np.max(dst))
    if options.precise:
        dsti=np.uint16(np.rint(np.clip(65535.0*dst,0.0,65535.0)))
        mode='I;16'
    else:
        dsti=np.uint8(np.rint(np.clip(255.0*dst,0.0,255.0)))
        mode='L'

    print "Quantised sinogram image range %d to %d" % (np.min(dsti),np.max(dsti))
    
    #Image.fromarray(dsti)
    Image.fromstring(mode,(dsti.shape[1],dsti.shape[0]),dsti.tostring()).save(args[1])

if __name__ == "__main__":

    main()
