#!/usr/bin/env python

import cairo
import cairo
import Image
import ImageDraw
import ImageFont
import math
import numpy as np
from optparse import OptionParser
import rsvg
import sys

# See http://stackoverflow.com/questions/7587699/how-to-render-fast-text-on-path
def warpPath(ctx, function):
    first = True

    for type, points in ctx.copy_path():
        if type == cairo.PATH_MOVE_TO:
            if first:
                ctx.new_path()
                first = False
            x, y = function(*points)
            ctx.move_to(x, y)

        elif type == cairo.PATH_LINE_TO:
            x, y = function(*points)
            ctx.line_to(x, y)

        elif type == cairo.PATH_CURVE_TO:
            x1, y1, x2, y2, x3, y3 = points
            x1, y1 = function(x1, y1)
            x2, y2 = function(x2, y2)
            x3, y3 = function(x3, y3)
            ctx.curve_to(x1, y1, x2, y2, x3, y3)

        elif type == cairo.PATH_CLOSE_PATH:
            ctx.close_path()


# Create the "mystery object"
def main():

    parser = OptionParser()  # TODO: Upgrade to argparser with Py 2.7
    parser.add_option("-g","--target",dest="target",help="Create TGT type",type='string',metavar="TGT",default='default')
    parser.add_option("-s","--small",action="store_true",dest="small",help="Output small target",default=False)
    parser.add_option("-t","--tiny",action="store_true",dest="tiny",help="Output tiny target",default=False)
    (options,args) = parser.parse_args()
    if len(args) != 1:
        sys.exit('Usage: mktarget [options] <dst.png>')

    s=(256,256)

    if options.target == 'default':
        
        # 5 layers:
        #   a background with a constant ellipse
        #   a tux
        #   some text
        #   a frequency response test pattern
        #   a scipy logo
        # then reduce and monochrome

        S=(2*s[0],2*s[1])
        layer0=Image.new("RGB",S)
        layer0.paste((0,0,0),(0,0,S[0],S[1]))
        
        layer1=Image.new("RGBA",S)
        draw = ImageDraw.Draw(layer1)
        draw.ellipse([(16,16),(S[0]-16,S[1]-16)],outline=(64,64,64,255),fill=(64,64,64,255))

        layer2=Image.new("RGBA",S)
        layer2.paste((0,0,0,0),(0,0,S[0],S[1]))
        tux=Image.open('input/tux.png')
        layer2.paste(tux,(100,100))

        image=Image.composite(layer1,layer0,layer1)
        image=Image.composite(layer2,image,layer2)

        surface=cairo.ImageSurface(cairo.FORMAT_ARGB32,S[0],S[1])
        ctx = cairo.Context(surface)
        ctx.set_source_rgb(0.75,0.75,0.75)
        ctx.select_font_face("Sans",cairo.FONT_SLANT_NORMAL,cairo.FONT_WEIGHT_BOLD)
        ctx.set_font_size(76)
        ctx.new_path()
        ctx.move_to(0,0)
        ctx.text_path('reconstruct me if you can')

        def warpfn(x,y):
            c=0.5*S[0]
            r=0.42*S[0]
            return 0.5*S[0]+(r+y)*math.sin(x/180.0),0.5*S[0]+(r+y)*math.cos(x/180.0)

        warpPath(ctx,warpfn)
        ctx.fill()
        layer3=Image.frombuffer("RGBA",(surface.get_width(),surface.get_height() ),surface.get_data(),"raw","RGBA",0,1)
        image=Image.composite(layer3,image,layer3)

        layer4=Image.new("RGBA",S)
        layer4.paste((0,0,0,0),(0,0,S[0],S[1]))
        for x in xrange(100,512-100):
            d=(x-100.0)
            a=int(255*(0.5-0.5*math.cos(d*d/1000.0)))
            for y in xrange(240,512-210):
                # layer4.putpixel((x,y),(192,192,192,a))
                layer4.putpixel((x,y),(a,a,a,255))
        image=Image.composite(layer4,image,layer4)

        svg=rsvg.Handle(file='input/scipy.svg')
        surface=cairo.ImageSurface(cairo.FORMAT_ARGB32,S[0],S[1])
        ctx=cairo.Context(surface)
        ctx.translate(0.6*S[0],0.275*S[1])
        ctx.scale(0.15,0.15)

        ctx.set_source_rgb(0.8,0.8,0.8)
        ctx.arc(0.5*S[0],0.5*S[1]-5.0,320.0,0.0,2.0*math.pi)
        ctx.fill()
        
        svg.render_cairo(ctx)
        surface.write_to_png("tmp.png")        
        layer5=Image.frombuffer("RGBA",(surface.get_width(),surface.get_height() ),surface.get_data(),"raw","RGBA",0,1)
        image=Image.composite(layer5,image,layer5)

        image=image.resize(s,Image.ANTIALIAS).convert('L')

    elif options.target == 'numbers':
        image=Image.new("L",s)
        draw = ImageDraw.Draw(image)
        draw.ellipse([(16,16),(240,240)],outline=64,fill=64)
        draw.rectangle([(150,50),(200,210)],outline=32,fill=32)
        draw.rectangle([(50,50),(100,210)],outline=96,fill=96)
        #font = ImageFont.truetype("/usr/share/fonts/truetype/msttcorefonts/Courier_New_Bold.ttf",64)
        #font = ImageFont.truetype("/usr/share/fonts/truetype/msttcorefonts/Arial_Bold.ttf",64)
        font = ImageFont.truetype("/usr/share/fonts/truetype/msttcorefonts/Comic_Sans_MS.ttf",50)
        draw.text((35, 45), "123456", font=font, fill=255)
        draw.text((35, 95), "789012", font=font, fill=255)
        draw.text((35,145), "345678", font=font, fill=255)
    elif options.target == 'solid':
        r,c=np.meshgrid(range(s[0]),range(s[1]))
        y=r-0.5*(s[0]-1)
        x=c-0.5*(s[1]-1)
        dst=np.less(y*y+x*x,125*125)*0.5
        dsti=np.uint8(np.rint(np.clip(255.0*dst,0.0,255.0)))
        image=Image.fromarray(dsti)
    else:
        sys.exit('Unrecognized target type')

    if options.small:
        image=image.resize((s[0]/2,s[1]/2),Image.ANTIALIAS)
    elif options.tiny:
        image=image.resize((s[0]/4,s[1]/4),Image.ANTIALIAS)

    image.save(args[0])

if __name__ == "__main__":        
    main()
