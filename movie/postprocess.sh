#!/bin/bash

convert sinogram-???.tiff -delay 5 -gravity center -background white -extent 256x360 sinogram.gif
convert -delay 5 recon-???.png recon.gif

F=`ls sinogram-???.tiff | sed 's/sinogram-//' | sed 's/.tiff//'`

for f in $F ; do convert -gravity center -background white -extent 256x360 sinogram-$f.tiff sinogram-$f-pad.png ; done
for f in $F ; do convert -gravity center -background white -extent 256x360 recon-$f.png recon-$f-pad.png ; done
for f in $F ; do convert sinogram-$f-pad.png recon-$f-pad.png +append movie-$f.png ; done

convert -delay 5 movie-???.png movie.gif
