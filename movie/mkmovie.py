#!/usr/bin/env python

import subprocess

def main():

    for a in range(1,360,2):
        print "%d angles" % a
        f="sinogram-%03d.tiff" % a

        s=subprocess.call(
            [
                "../generate/mksinogram.py",
                "-p",
                "-a",
                "%d" % a,
                "../generate/target.png",
                f
                ]
            )

        s=subprocess.call(
            [
                "../recon-fbp/mkrecon.py",
                "-e",
                "0.0",
                f,
                "recon-%03d.png" % a
                ]
            )

if __name__ == "__main__":
    main()
