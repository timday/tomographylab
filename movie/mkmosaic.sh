#!/bin/bash

F=`ls recon-???.png | awk '{if (NR==1 || NR==2 || NR==3 || NR==5 || NR==8 || NR==13 || NR==21 || NR==34 || NR==55 || NR==89 || NR==144 || NR==233) print $0;}'`

montage -tile 11x1 $F recon-montage.png
