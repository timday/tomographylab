import numpy as np
import scipy
import scipy.sparse

# Efficiently accumulate a multiple of a sparse matrix onto a dense one.  No return
def accumulate(sparse,k,dense):
    ri=np.repeat(
        np.arange(sparse.shape[0]),
        np.diff(sparse.indptr)
        )
    dense[ri,sparse.indices]+=k*sparse.data

# Return result of multiplying a sparse matrix by an array interpreted as a dense column vector
def dot(sparse,dense):
    ri=np.repeat(
        np.arange(sparse.shape[0]),
        np.diff(sparse.indptr)
        )
    return np.sum(dense[ri,sparse.indices]*sparse.data)
