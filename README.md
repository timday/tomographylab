Tomographylab
=============

Experiments with tomographic reconstruction.
This is *not* intended to be a polished, "finished product".
The code is good enough to convince the author he had a reasonable understanding of
the algorithms described, and to produce interesting material for a poster on the topic
(if the project has any sort of "final output", it's the poster at <http://timday.bitbucket.org/tomographylab-poster.pdf>).

The important stuff is the generate and recon- implementations, and the poster.
Most directories have makefiles to run their experiments,
although exactly what's configured to run may need attention.

Main contents:

* generate: Creates sinogram (Radon transform) for testing.
* recon-fbp: Filtered back projection reconstruction
* recon-art: Algebraic reconstruction technique
* recon-sart: SART/SIRT-like iterative technique
* recon-quad: Direct least squares solution (NB needs an enormous amount of RAM and time)
* poster: Description of the project

Extras:

* analyse: Creates some plots for accuracy of reconstruction.
* colour: Sinograms and FBP on colour images (for fun/as an "effect") ?  Not progressed.
* experiment: Experiments to check some python/scipy things did what's expected.
* lib: Helper code
* movie: Fold reconstruction outputs into a movie (animated GIF)

Licensing
---------
Copyright 2012-2013 Tim Day.
This project's code is open to inspection in a public repository in the hope it will 
be of interest and value to others contemplating or undertaking a similar endeavour.

References
----------

Besides the books referenced on the poster, the following were also of some interest:

* Superb overview of reconstruction algorithms at <http://www.dspguide.com/ch25/5.htm>
* Comments on central slice theorem / fourier slice theorem / projection slice theorem at <http://hobbieroth.blogspot.co.uk/2011/05/central-slice-theorem-and-ronald.html>.
* Material on direct Fourier reconstruction: <http://www.math.upenn.edu/~trietle/teaching/math584/notes/week9_2.pdf> and <http://code.google.com/p/centralslice/>

Statistical (e.g expectation maximisation algorithms) have yet to be implemented here
(and would be the obvious next target):

* (S)MART and EMML at <http://faculty.uml.edu/cbyrne/twoalgs.pdf>
* Good taxonomy of reconstruction algorithms <http://web.eecs.umich.edu/~fessler/papers/files/talk/02/mic,slide.pdf> (also <http://web.eecs.umich.edu/~fessler/papers/files/talk/02/mic,notes.pdf>)
