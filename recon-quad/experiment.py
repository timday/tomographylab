#!/usr/bin/env python

import cPickle as pickle
import numpy as np
import scipy.sparse
import time

def loadp():
    filename='projections.pkl'
    with open(filename,'r') as f:
        proj=pickle.load(f)
        print 'Loaded {0}: {1}'.format(filename,proj.info())
    
    return scipy.sparse.vstack(
        (
            scipy.sparse.csr_matrix(p.toarray().reshape((1,65536)),dtype=np.float32)
            for p in proj.projections
            )
        )

def inspect():
    p=loadp()
    print type(p)
    print 'Projections shape {0}, dtype {1}, density {2:.2}%'.format(
        p.shape,p.dtype,(100.0*p.data.shape[0])/(p.shape[0]*p.shape[1])
        )
    # Reports (91904, 65536), 0.62%

    # This fails with negative dimensions are not allowed
    # p=p.T*p 
    # print 'Multiplied shape {0}, density {1:.2}%'.format(p.shape,(100.0*p.data.shape[0])/(p.shape[0]*p.shape[1]))

    # Therefore need to do
    p=p.toarray() # but fails with memory error on namche.  Should be 22.4375 GByte.

    print "Flattened"
    p=np.dot(p.T,p)
    print "Multiplied"
    
    # Try alternative approach using http://docs.scipy.org/doc/scipy/reference/sparse.linalg.html ?
    # lsqr? Has some regularization
    # lsmr? Ditto (may neet too recent a version).

#def simulate():
#    p=scipy.sparse.

def main():
    inspect()
    #simulate()

if __name__ == "__main__":
    main()

