class Projections:

    def __init__(self,shape,projections,norm2s):
        self.shape=shape
        self.projections=projections
        self.norm2s=norm2s

    def info(self):
        assert len(self.projections)==len(self.norm2s)

        # Count the number of occupied and unoccupied elements
        n=sum((x.data.shape[0] for x in self.projections))
        m=sum((x.shape[0]*x.shape[1] for x in self.projections))

        return 'projections for sinogram size {0}, {1} projections of size {2} with density {3:.2}%'.format(
            self.shape,
            len(self.projections),
            self.projections[0].shape,
            (100.0*n)/m
            )
