#!/usr/bin/env python

import cPickle as pickle
import Image
import matplotlib.pyplot as plt
import math
from multiprocessing import Pool
import numpy as np
from optparse import OptionParser
import projections
import scipy.linalg
import scipy.sparse
import scipy.sparse.linalg
import sys
import time

import sys
sys.path.append('../lib/')
import sparsedense

# Progress indicator
def dot():
    sys.stdout.write('.')
    sys.stdout.flush()

def sqr(x):
    return x*x

def matrix_density(m):
    return len(np.flatnonzero(m))/float(m.size)
#    return np.count_nonzero(m)/float(m.size)   # TODO: Needs more recent numpy

def reconstruct(options,filename_src,filename_proj,filename_dst):
    # Read sinogram and normalize range
    src=Image.open(filename_src)
    if src.mode=='L':
        src=np.asarray(src)/255.0
    elif src.mode=='I;16':
        #src=np.asarray(src)/65535.0 # Doesn't work: need to do this workround instead
        src=np.fromstring(src.tostring(),np.uint16).reshape((src.size[1],src.size[0]))/65535.0
    else:
        exit('Unexpected image file type')

    s=(src.shape[1],src.shape[1])

    # Convert back to accumulated material estimate
    src = np.array(-options.k*np.log(src),dtype=np.float32)

    t0=time.time()
    with open(filename_proj,'r') as f:
        proj=pickle.load(f)
    print 'Loaded {0}: {1}'.format(filename_proj,proj.info())
    if proj.shape!=src.shape:
        exit('{0} inconsistent with number of sinogram rows').format(filename_proj)
    t1=time.time()
    print "Loaded {0} in {1} seconds".format(filename_proj,t1-t0)

    # Going to convert to overdetermined linear system
    # S = P T
    # by solving (least squares)
    # P.T S = (P.T P) T

    # Smooth version per http://en.wikipedia.org/wiki/Ridge_regression
    # "Tikhonov regularization"
    # T = (P.T P + L.T L)^-1 . P.T S
    # ie
    # (P.T P + L.T L) T = P.T S
    # ie the same, but add in some extra 
    
    S=src.reshape((src.shape[0]*src.shape[1],))
    
    if options.gigantic:
        P=np.vstack(
            (
                p.toarray().reshape((1,s[0]*s[1]))
                for p in proj.projections
                )
            )
    else:
        P=scipy.sparse.vstack(
            (
                scipy.sparse.csr_matrix(p.toarray().reshape((1,s[0]*s[1])),dtype=np.float32)
                for p in proj.projections
                )
            )

    print "Type of P is {0} dtype {1}".format(type(P),P.dtype)

    proj=None

    T=np.zeros((s[0]*s[1],))
    
    print "System: {0} = {1}.{2}".format(S.shape,P.shape,T.shape)
    
    if options.gigantic:
        print "Computing P^T.S..."
        PtS=np.dot(P.T,S)
        print "Computing P^T.S..."
        PtP=np.dot(P.T,P)
        print "...computed"
        P=None
        Q=PtP
    else:
        PtS=P.T*S
        PtP=P.T*P
        P=None
        print "PtP: {0} {1:.2}%, ".format(PtP.shape,(100.0*PtP.data.shape[0])/(PtP.shape[0]*PtP.shape[1]))
        print "Unpacking"
        Q=PtP.toarray()      # PtP is 68% occupied... so hardly worth carrying on sparse?
        PtP=None
    
    dudrows=filter(lambda i: (np.sum(Q[i,:])<=1e-3),xrange(Q.shape[0]))
    dudcols=filter(lambda i: (np.sum(Q[:,i])<=1e-3),xrange(Q.shape[1]))
    assert dudrows == dudcols
    print "Dud pixels: {0}".format(len(dudrows))
    duds=frozenset(dudrows)
    
    QtoR={}
    for i in xrange(Q.shape[0]):
        if not i in duds:
            QtoR[i]=len(QtoR)

    RtoQ=dict([(p[1],p[0]) for p in QtoR.items()])

    # Validate
    for i in xrange(s[0]*s[1]):
        if not i in duds:
            j=QtoR[i]
            assert RtoQ[j] == i

    rowmask=np.array([not i in dudrows for i in xrange(Q.shape[0])])
    colmask=np.array([not i in dudcols for i in xrange(Q.shape[1])])

    print "Compressing"
    R=np.compress(colmask,np.compress(rowmask,Q,axis=0),axis=1)
    Qshape=Q.shape
    Q=None
    CPtS=np.compress(colmask,PtS)

    # Smoothing options.
    # Scaling of identity matrix just acts minimise the norm of all unknowns
    if options.smooth_norm:
        print "Smoothing norm (factor {0})".format(options.smooth_factor)
        f2=sqr(options.smooth_factor)
        for i in xrange(R.shape[0]):
            R[i,i]+=f2

    # An alternative is to minimise norm of diff with neighbours
    # Build a sparse regularization matrix
    if options.smooth_diff:
        print "Smoothing diff (factor {0})".format(options.smooth_factor)

        # Gamma matrix is applied to the target vector; norm of that is minimised
        # Gamma^T.Gamma is added into least squares expression
        gamma=scipy.sparse.dok_matrix((s[0]*s[1],s[0]*s[1]),dtype=np.float32)

        # TODO: This is just diagonal; needs extending to neighbours
        print 'Constructing gamma'
        for i in xrange(1,s[0]-1):
            for j in xrange(1,s[1]-1):
                c=i*s[1]+j
                left=i*s[1]+j-1
                right=i*s[1]+j+1
                up=(i-1)*s[1]+j
                down=(i+1)*s[1]+j
                gamma[c,c]+=options.smooth_factor
                for neighbour in [left,right,up,down]:
                    gamma[c,neighbour]-=0.25*options.smooth_factor

        print 'Multiplying gamma'
        # Do the least-squares thing (multiply yields a CSR matrix)
        gamma=(gamma.T*gamma)

        print 'Compressing gamma'
        # Eliminate duds
        c=zip(*gamma.nonzero())
        c=zip(c,gamma.data)
        c=filter(lambda x: not (x[0][0] in duds or x[0][1] in duds),c)
        gamma=map(lambda x: ((QtoR[x[0][0]],QtoR[x[0][1]]),x[1]),c)
        c,d=zip(*gamma)
        rows,cols=zip(*c)

        print 'Accumulating gamma'
        # Accumulate onto R
        R[rows,cols]+=d

        # Cleanup
        gamma=None

    print "R range {0} - {1}".format(np.min(R),np.max(R))
    
    print 'Increasing precision'
    R=np.float64(R)
    CPtS=np.float64(CPtS)

    print 'Matrix density {0:.2}%'.format(100.0*matrix_density(R))

    print 'Solving'
    # Matrix multiplied by its transpose is symmetric; less clear that it's positive definite though.
    # Something in those options helping it run in 10 rather than 15 mins though
    # Permutation breaks this though
    
    t0=time.clock()
    CT=scipy.linalg.solve(R,CPtS,overwrite_a=True,overwrite_b=True)
    t1=time.clock()
    print '...solved in {0}s'.format(t1-t0)

    print 'CT: range {0} to {1}'.format(np.min(CT),np.max(CT))

    T=np.zeros((s[0]*s[1],))
    for i in xrange(len(T)):
        if not i in duds:
            j=QtoR[i]
            T[i]=CT[j]
    
    dst=T.reshape(s)
    dsti=np.uint8(np.rint(np.clip(255.0*dst,0.0,255.0)))
    Image.fromarray(dsti).save(filename_dst)

def main():

    parser = OptionParser()  # TODO: Upgrade to argparser with Py 2.7

    parser.add_option("-g","--gigantic",action="store_true",dest="gigantic",help="Gigantic memory",default=False)
    parser.add_option("-p","--plot",action="store_true",dest="plot",help="Show some plots",default=False)
    parser.add_option("-s","--smooth-norm",action="store_true",dest="smooth_norm",help="Smooth norm",default=False)
    parser.add_option("-d","--smooth-diff",action="store_true",dest="smooth_diff",help="Smooth diff",default=False)
    parser.add_option("-f","--smooth-factor",dest="smooth_factor",help="Smoothing factor",metavar="<f>",type="float",default=1.0)
    parser.add_option("-k","--k-coefficient",dest="k",help="Attenuation coefficient",type='float',metavar="K",default=64.0)

    (options,args) = parser.parse_args()

    if len(args) != 3:
        sys.exit('Usage: mkrecon [options] <src> <projections> <dst>')

    reconstruct(options,args[0],args[1],args[2])

    if options.plot:
        plt.show()

if __name__ == "__main__":
    main()
