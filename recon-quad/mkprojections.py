#!/usr/bin/env python

import cPickle as pickle
import Image
import itertools
import math
from multiprocessing import Pool
import numpy as np
from optparse import OptionParser
import projections
import scipy.ndimage.interpolation
import scipy.sparse
import sys

# Progress indicator
def dot():
    sys.stdout.write('.')
    sys.stdout.flush()

s=None

# For each sinogram pixel, we construct a (sparse) matrix containing
# the weighting of each target pixel to that output pixel.
# This is pretty horrendously inefficient brute force construction;
# ideally what's needed is the ability to rotate a sparse matrix efficiently.
class Projector:

    def __init__(self,sinogram_shape):
        self._sinogram_shape=sinogram_shape
        y,x=np.meshgrid(range(s[0]),range(s[1]))
        y-=0.5*(s[0]-1)
        x-=0.5*(s[1]-1)
        r2=x*x+y*y
        self._mask=np.array((r2<0.25*s[0]*s[0]),dtype=np.float32)
    
    def __call__(self,sinogram_pixel):
        sinogram_row = sinogram_pixel/self._sinogram_shape[1]
        sinogram_col = sinogram_pixel%self._sinogram_shape[1]
        if sinogram_col==0: 
            dot()
        assert 0<=sinogram_row and sinogram_row<self._sinogram_shape[0]
        a=sinogram_row*360.0/self._sinogram_shape[0]
        m=np.zeros(s,dtype=np.float32)
        m[sinogram_col,:]=self._mask[sinogram_col,:]
        m=scipy.ndimage.interpolation.rotate(m,-a,order=1,reshape=False,mode='constant',cval=0.0)  # Negate angle because we are rotating scanner space to target space
        n=np.sum(m*m)
        assert n != 0.0
        m=scipy.sparse.csr_matrix(m,dtype=np.float32)
        return (m,n)
    
def make_projections(options,filename_src,filename_dst):

    global s

    # Read sinogram and normalize range (although really the only thing we care about here is the size)
    src=Image.open(filename_src)
    if src.mode=='L':
        src=np.asarray(src)/255.0
    elif src.mode=='I;16':
        #src=np.asarray(src)/65535.0 # Doesn't work: need to do this workround instead
        src=np.fromstring(src.tostring(),np.uint16).reshape((src.size[1],src.size[0]))/65535.0
    else:
        exit('Unexpected image file type')

    s=(src.shape[1],src.shape[1])

    print 'Creating projections...'
    projector=Projector(src.shape)
    pool=Pool()
    proj=pool.map(projector,xrange(src.shape[0]*src.shape[1]))
    print
    proj=projections.Projections(src.shape,[p[0] for p in proj],[p[1] for p in proj])
    print '...created projections: {0}; dumping projections...'.format(proj.info())
    with open(filename_dst,'w') as f:
        pickle.dump(proj,f,pickle.HIGHEST_PROTOCOL)
    print '...dumped projections'

def main():
    parser = OptionParser()  # TODO: Upgrade to argparser with Py 2.7
    (options,args) = parser.parse_args()
    if len(args) != 2:
        sys.exit('Usage: mkprojections [options] <src> <dst>')

    make_projections(options,args[0],args[1])

if __name__ == "__main__":
    main()
