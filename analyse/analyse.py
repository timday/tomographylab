#!/usr/bin/env python

import Image
import matplotlib.pyplot as plt
import numpy as np
from optparse import OptionParser
import re
import sys

def main():
    parser = OptionParser()
    (options,args) = parser.parse_args()

    if len(args) < 2:
        sys.exit('Must specify at least a reference and one compared image')

    tgt=np.array(Image.open(args[0]),dtype=np.float)

    mean=[]
    std=[]
    a=[]
    for f in args[1:]:
        img=np.array(Image.open(f),dtype=np.float)

        err=(img-tgt).flatten()
        mean.append(np.mean(err))
        std.append(np.std(err))
        n=int(re.sub('[^0-9]','',f))
        a.append(n)

    plt.plot(a,mean,label='Mean',color='g')
    plt.plot(a,std,label='Standard deviation',color='r')
    plt.title('Reconstruction error')
    plt.xlabel('Number of projections')
    plt.ylabel('Image greylevel differences')
    plt.legend()
    plt.show()

if __name__ == "__main__":
    main()
