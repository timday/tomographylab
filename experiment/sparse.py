#!/usr/bin/env python

from itertools import chain
import numpy as np
import numpy.random
import scipy
import scipy.sparse
import time

N=512
S=512
D=1

def mkrandomsparse():
    m=np.zeros((S,S),dtype=np.float32)
    r=np.random.random_integers(0,S-1,D*S)
    c=np.random.random_integers(0,S-1,D*S)
    for e in zip(r,c):
        m[e[0],e[1]]=1.0
    return scipy.sparse.csr_matrix(m)

M=[mkrandomsparse() for i in xrange(N)]

def plus_dense():
    return reduce(lambda x,y: x+y,(m.toarray() for m in M))

def plus_sparse():
    return reduce(lambda x,y: x+y,M).toarray()

def sum_dense():
    return sum((m.toarray() for m in M))

def sum_sparse():
    return sum(M[1:],M[0]).toarray()

def sum_combo():  # Sum the sparse matrices 'onto' a dense matrix?
    return sum(M,np.zeros((S,S),dtype=np.float32))

def sum_combo_onto():
    r=M[0].toarray()
    for m in M[1:]:
        r+=m
    return r

# Accumulate the sparse matrix onto the dense one.  No return
def _acc(dense,sparse):
    ri=np.repeat(
        np.arange(sparse.shape[0]),
        np.diff(sparse.indptr)
        )
    dense[ri,sparse.indices]+=sparse.data

def acc():
    r=np.zeros((S,S))
    map(lambda x: _acc(r,x),M)
    return r

def benchmark(fn):
    t0=time.time()
    v=fn()
    t1=time.time()
    print "{0:16}:  {1:.3f}s   {2}".format(fn.__name__,t1-t0,np.sum(v))
    
for i in xrange(4):
    benchmark(plus_dense)
    benchmark(plus_sparse)
    benchmark(sum_dense)
    benchmark(sum_sparse)
    benchmark(sum_combo)
    benchmark(sum_combo_onto)
    benchmark(acc)
    print
