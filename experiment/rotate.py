#!/usr/bin/env python

import numpy as np

import scipy.ndimage.interpolation

# Was supposed to prove that scipy rotation is summetric and therefore around
# the point 1.5 in 0,1,2,3.  Or (n-1)/2 in an nxn matrix.
# Well, it does for a=45, but then goes a bit strange at a=90, 180...
# ...unless you're careful with options.  Order=1 seems to be crucial?

a=np.zeros((4,4))
a[1:3,1:3]=1
print a
print

b=scipy.ndimage.interpolation.rotate(a,180,order=1,reshape=False,prefilter=False,mode='constant',cval=0.0)

print b
print
