#!/usr/bin/env python

import numpy as np
import scipy
import scipy.sparse

P=scipy.sparse.dok_matrix((90000,65536),dtype=np.float32)

#P[0:90000,  0    ]=1.0
#P[0:90000,  65535]=1.0
#P[      0,0:65536]=1.0
P[  89999,0:65535]=1.0

P=scipy.sparse.csr_matrix(P)

print P.shape

Pt=P.T

print Pt.shape

Q=Pt*P

print Q.shape

# Apparently limited to 2 giga elements total number of elements
# pending of 64bitification of scipy.sparse
